package ulco.cardGame.server;

import ulco.cardGame.common.games.CardGame;
import ulco.cardGame.common.games.PokerGame;
import ulco.cardGame.common.interfaces.Game;
import ulco.cardGame.common.interfaces.Player;
import ulco.cardGame.common.players.CardPlayer;
import ulco.cardGame.common.players.PokerPlayer;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.Scanner;

public class CardGameServer {

    private ServerSocket server;
    protected Game game;
    protected Map<Player, Socket> playerSockets;
    protected Constructor playerConstructor;

    public static final int PORT = 7777;

    public static void main(String[] args) {
        Game game = null;
        Constructor playerConstructor = null;

        // Get expected game name of user
        System.out.println("Which game do you want to play ?");
        Scanner scanner = new Scanner(System.in);

        String gameName = scanner.nextLine();

        try {
            if (gameName.equals("Battle")) {
                // Need to specify your current cardGame.txt file
                game = new CardGame("Battle", 3, "resources/games/cardGame.txt");
                playerConstructor = CardPlayer.class.getConstructor(String.class);
            } else if (gameName.equals("Poker")) {
                game = new PokerGame("Texas Poker", 3, 3, "resources/games/pokerGame.txt");
                playerConstructor = PokerPlayer.class.getConstructor(String.class);
            }

            if (game == null) {
                System.out.println("Unknown Game...");
                return;
            }

            // add each player until game is ready (or number of max expected player is reached
            int userIndex = 0;

            do {
                // use of dynamic constructor (reflection concept) in order to instance expected user
                Player player = (Player) playerConstructor.newInstance("user" + userIndex);
                game.addPlayer(player);

                userIndex++;

            } while (!game.isStarted());

            // Game Loop
            // run the whole game using sockets
            Player winner = game.run();

            System.out.println("Winner of the game is " + winner + " !!");

            // remove all players from the game
            game.removePlayers();
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    private void run() {

        try {

            // Need to specify your current cardGame.txt file
            game = new CardGame("Battle", 3, "resources/games/cardGame.txt");

            // Game Loop
            System.out.println("Waiting for new player for " + game.toString());

            // add each player until game is ready (or number of max expected player is reached)
            // Waiting for the socket entrance
            do {

                Socket socket = server.accept();

                // read socket Data
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                String playerUsername = (String) ois.readObject();

                // Create player instance
                Player player = new CardPlayer(playerUsername);

                // TODO : Add player in game



                ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                oos.writeObject(game);

                // Tell to other players that new player is connected
                for (Socket playerSocket : playerSockets.values()) {

                    ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                    playerOos.writeObject("Now connected: " + player.getName());
                }

                // Store player's socket in dictionary (Map)
                playerSockets.put(player, socket);

                System.out.println(player.getName() + " is now connected...");


            } while (!game.isStarted());

            // run the whole game using sockets
            // TODO : Player gameWinner = game.run(playerSockets);

            // Tells to player that server will be closed (just as example)
            for (Socket playerSocket : playerSockets.values()) {
                ObjectOutputStream playerOos = new ObjectOutputStream(playerSocket.getOutputStream());
                playerOos.writeObject("END");
            }

            // Close each socket when game is finished
            for (Socket socket : playerSockets.values()) {
                socket.close();
            }

            game.removePlayers();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
